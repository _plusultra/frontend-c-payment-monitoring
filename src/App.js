import "antd/dist/antd.css";
import React from "react";
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from "react-query";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar";
import {
  ADCreateRolePage,
  ADManagementRolePage,
  ADUpdateRolePage,
} from "./pages/Admin";
import { GSLoginPage, GSPaymentRequestPage } from "./pages/GeneralSupport";
import {
  UKBerandaPage,
  UKDetailPaymentPage,
  UKLoginPage,
  UKPaymentRequestPage,
} from "./pages/UnitKerja";

// Create a client
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Switch>
          <Route exact path="/" component={LoginContainer} />
          <Route path="/officer-login" component={GSLoginPage} />
          <Route component={DefaultContainer} />
        </Switch>
      </Router>
    </QueryClientProvider>
  );
}

const LoginContainer = () => <Route path="/" component={UKLoginPage} />;

const DefaultContainer = () => (
  <>
    <Navbar />
    <Route path="/" exact component={UKLoginPage} />
    <Route path="/unitkerja-beranda" component={UKBerandaPage} />
    <Route path="/unitkerja-paymentrequest" component={UKPaymentRequestPage} />
    <Route path="/unitkerja-detailpayment" component={UKDetailPaymentPage} />
    <Route path="/gs-paymentrequest" component={GSPaymentRequestPage} />
    <Route path="/role-account-update" component={ADUpdateRolePage} />
    <Route path="/role-account-add" component={ADCreateRolePage} />
    <Route path="/admin-beranda" component={ADManagementRolePage} />
  </>
);

export default App;
