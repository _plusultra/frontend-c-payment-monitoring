import { Button, Row, Space, Table, Tag, Typography, Alert, Col } from "antd";
import { useQuery } from "react-query";
import moment from "moment";

import React from "react";
import { useHistory } from "react-router-dom";
import "./ListPaymentRequest.css";

const { Text } = Typography;

const ListPaymentRequest = () => {
  // Queries
  const { isLoading, isError, data, refetch } = useQuery("todos", async () => {
    const response = await fetch("http://localhost:5000/paymentRequest");
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    return response.json();
  });

  const history = useHistory();
  const navigateTo = React.useCallback(
    () => history.push("/unitkerja-detailpayment"),
    [history]
  );

  // table
  const columns = React.useMemo(
    () => [
      {
        title: "No",
        render: (_, __, index) => <Text>{index + 1}.</Text>,
      },
      {
        title: "Tanggal Request",
        dataIndex: "tgl_request",
        key: "tgl_request",
        render: (value) => {
          const formatedDate = moment(new Date(value)).format("DD MMMM YYYY hh:mm")
          return(<Text>{formatedDate} WIB</Text>)
        },
      },
      {
        title: "Tanggal Pembayaran",
        dataIndex: "tgl_pembayaran",
        key: "tgl_pembayaran",
        render: (value) => {
          const formatedDate = moment(new Date(value)).format("DD MMMM YYYY")
          return(<Text>{formatedDate}</Text>)
        },
      },
      {
        title: "Action",
        key: "action",
        dataIndex: "action",
        render: (action) => (
          <>
            {action.map((act) => {
              let color;
              if (act === "rejected by accounting") {
                color = "volcano";
              } else if (act === "disetujui") {
                color = "green";
              } else if (act === "menunggu konfirmasi") {
                color = "yellow";
              }
              return (
                <Tag color={color} key={act}>
                  {act.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
      },
      {
        // title: 'Button',
        key: "buttonDetail",
        render: (text) => (
          <Space size="middle">
            <Button type="primary" onClick={navigateTo}>
              Detail
            </Button>
          </Space>
        ),
      },
    ],
    [navigateTo]
  );

  return (
    <Row
      justify="center"
      style={{
        marginTop: "40px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Space direction="vertical">
        <h1>List Payment Request - KC Bank XXX</h1>
        {isError && (
          <Alert
            message="Gagal memuat data"
            showIcon
            type="error"
            action={
              <Col span={4}>
                <Button danger onClick={refetch}>
                  Muat ulang
                </Button>
              </Col>
            }
          />
        )}
        <Table
          loading={isLoading}
          rowClassName={(_, index) =>
            index % 2 === 0 ? "table-row-light" : "table-row-dark"
          }
          columns={columns}
          dataSource={data}
        ></Table>
      </Space>
    </Row>
  );
};

export default ListPaymentRequest;
